# Exercício Spring Boot/Data - Funcionários
Criar um CRUD para os seguintes tabelas do banco de dados, na respectiva ordem:

1- Localização (locations)
2- Departamento (departments)
3- Cargo (job)
4- Funcionário (employees)

## Requisitos
- Todos os campos e classes devem usar o nome em português.
- Deve-se respeitar a modelagem inicial das tabelas, sem alterar campos e tipos.
- Não é preciso criar CRUDs de tabelas que não foram citadas acima
- Fazer validação dos campos assim que possível